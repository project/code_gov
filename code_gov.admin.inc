<?php

/**
 * @file
 * Administrative page callbacks for code_gov.
 */

/**
 * Administrative form for code_gov.
 */
function _code_gov_admin_form() {
  $defaults = array(
    'measurementType' => NULL,
    'ifOther' => NULL,
    'agency' => NULL,
  );
  $saved = variable_get('code_gov_settings', array());
  if (!empty($saved['measurementType']['method'])) {
    $defaults['measurementType'] = $saved['measurementType']['method'];
  }
  if (!empty($saved['measurementType']['ifOther'])) {
    $defaults['ifOther'] = $saved['measurementType']['ifOther'];
  }
  if (!empty($saved['agency'])) {
    $defaults['agency'] = $saved['agency'];
  }

  $form['measurementType'] = array(
    '#required' => TRUE,
    '#default_value' => $defaults['measurementType'],
    '#title' => t('Measurement type'),
    '#description' => t('The method for measuring the open source requirement.'),
    '#type' => 'select',
    '#options' => array(
      'linesOfCode' => t('Source lines of code'),
      'modules' => t('Number of self-contained modules'),
      'cost' => t('Cost of software development'),
      'projects' => t('Number of software projects'),
      'systems' => t('System certification and accreditation boundaries'),
      'other' => t('Another measurement method not referenced above.'),
    ),
  );
  $form['ifOther'] = array(
    '#default_value' => $defaults['ifOther'],
    '#title' => t('Measurement type details'),
    '#description' => t('A one or two-sentence description of the measurement type used.'),
    '#type' => 'textfield',
    '#states' => array(
      'visible' => array(
        ':input[name="measurementType"]' => array('value' => 'other'),
      ),
    ),
  );
  $form['agency'] = array(
    '#required' => TRUE,
    '#default_value' => $defaults['agency'],
    '#title' => t('Agency'),
    '#description' => t('The agency acronym for Clinger Cohen Act agency, as defined by the United States Government Manual. For example "GSA" or "DOD."'),
    '#type' => 'textfield',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit callback for _code_gov_admin_form.
 */
function _code_gov_admin_form_submit($form, $form_state) {
  $input = $form_state['input'];
  $settings = array(
    'version' => '2.0.0',
  );
  $settings['measurementType'] = array(
    'method' => $input['measurementType'],
    'ifOther' => $input['ifOther'],
  );
  $settings['agency'] = $input['agency'];
  variable_set('code_gov_settings', $settings);

  // Make sure to clear the cached version.
  cache_clear_all('code_gov_json', 'cache', TRUE);
}
