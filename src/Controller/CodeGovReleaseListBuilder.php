<?php

/**
 * @file
 * Class for listing the Code.gov releases.
 */

namespace Drupal\code_gov\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;

/**
 * Provides a listing of Code.gov releases.
 */
class CodeGovReleaseListBuilder extends ConfigEntityListBuilder {

  /**
  * {@inheritdoc}
  */
  public function buildHeader() {
    $header['label'] = $this->t('Code.gov release');
    $header['id'] = $this->t('Machine name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $this->getLabel($entity);
    $row['id'] = $entity->id();

    return $row + parent::buildRow($entity);
  }
}
