<?php

/**
 * @file
 * Contains CodeGovJsonController class.
 */

namespace Drupal\code_gov\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller class for the code.json output.
 */
class CodeGovJsonController extends ControllerBase {

  /**
   * Callback for json output.
   */
  public function jsonRender(Request $request) {
    $response['data'] = 'foo';
    $response['method'] = 'GET';
    return new JsonResponse($response);
  }
}
