<?php

/**
 * @file
 * Base class for code_gov_release entities.
 */

namespace Drupal\code_gov\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\code_gov\CodeGovReleaseInterface;

/**
 * Defines the code_gov_release entity.
 *
 * @ConfigEntityType(
 *   id = "code_gov_release",
 *   label = @Translation("Code.gov Release"),
 *   handlers = {
 *     "list_builder" = "Drupal\code_gov\Controller\CodeGovReleaseListBuilder",
 *     "form" = {
 *       "add" = "Drupal\code_gov\Form\CodeGovReleaseForm",
 *       "edit" = "Drupal\code_gov\Form\CodeGovReleaseForm",
 *       "delete" = "Drupal\code_gov\Form\CodeGovReleaseDeleteForm",
 *     }
 *   },
 *   config_prefix = "release",
 *   admin_permission = "administer code_gov",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/search/code-gov/release/{code_gov_release}",
 *     "delete-form" = "/admin/config/search/code-gov/release/{code_gov_release}/delete",
 *   }
 * )
 */
class CodeGovRelease extends ConfigEntityBase implements CodeGovReleaseInterface {

  /**
   * The release ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The release label.
   *
   * @var string
   */
  protected $label;

  /**
   * The release repositoryURL.
   *
   * @var string
   */
  protected $repositoryURL;

  /**
   * The release description.
   *
   * @var string
   */
  protected $description;

  /**
   * The release usageType.
   *
   * @var string
   */
  protected $usageType;

  /**
   * The release exemptionText.
   *
   * @var string
   */
  protected $exemptionText;

  /**
   * The release laborHours.
   *
   * @var string
   */
  protected $laborHours;

  /**
   * The release tags.
   *
   * @var string
   */
  protected $tags;

  /**
   * The release contactEmail.
   *
   * @var string
   */
  protected $contactEmail;

  /**
   * The release licenses.
   *
   * @var string
   */
  protected $licenses;

  /**
   * The release version.
   *
   * @var string
   */
  protected $version;

  /**
   * The release organization.
   *
   * @var string
   */
  protected $organization;

  /**
   * The release status.
   *
   * @var string
   */
  protected $releaseStatus;

  /**
   * The release vcs.
   *
   * @var string
   */
  protected $vcs;

  /**
   * The release homepageURL.
   *
   * @var string
   */
  protected $homepageURL;

  /**
   * The release .
   *
   * @var string
   */
  protected $downloadURL;

  /**
   * The release disclaimerText.
   *
   * @var string
   */
  protected $disclaimerText;

  /**
   * The release disclaimerURL.
   *
   * @var string
   */
  protected $disclaimerURL;

  /**
   * The release languages.
   *
   * @var string
   */
  protected $languages;

  /**
   * The release contactName.
   *
   * @var string
   */
  protected $contactName;

  /**
   * The release contactURL.
   *
   * @var string
   */
  protected $contactURL;

  /**
   * The release contactPhone.
   *
   * @var string
   */
  protected $contactPhone;

  /**
   * The release partners.
   *
   * @var string
   */
  protected $partners;

  /**
   * The release relatedCode.
   *
   * @var string
   */
  protected $relatedCode;

  /**
   * The release reusedCode.
   *
   * @var string
   */
  protected $reusedCode;

  /**
   * The release created.
   *
   * @var string
   */
  protected $created;

  /**
   * The release lastModified.
   *
   * @var string
   */
  protected $lastModified;

  /**
   * The release metadataLastUpdated.
   *
   * @var string
   */
  protected $metadataLastUpdated;



  /**
   * {@inheritdoc}
   */
  public function getRepositoryURL() {
    return $this->repositoryURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getUsageType() {
    return $this->usageType;
  }

  /**
   * {@inheritdoc}
   */
  public function getExemptionText() {
    return $this->exemptionText;
  }

  /**
   * {@inheritdoc}
   */
  public function getLaborHours() {
    return $this->laborHours;
  }

  /**
   * {@inheritdoc}
   */
  public function getTags() {
    return $this->tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactEmail() {
    return $this->contactEmail;
  }

  /**
   * {@inheritdoc}
   */
  public function getLicenses() {
    return $this->licenses;
  }

  /**
   * {@inheritdoc}
   */
  public function getVersion() {
    return $this->version;
  }

  /**
   * {@inheritdoc}
   */
  public function getOrganization() {
    return $this->organization;
  }

  /**
   * {@inheritdoc}
   */
  public function getStatus() {
    return $this->releaseStatus;
  }

  /**
   * {@inheritdoc}
   */
  public function getVcs() {
    return $this->vcs;
  }

  /**
   * {@inheritdoc}
   */
  public function getHomepageURL() {
    return $this->homepageURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDownloadURL() {
    return $this->downloadURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisclaimerText() {
    return $this->disclaimerText;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisclaimerURL() {
    return $this->disclaimerURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguages() {
    return $this->languages;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactName() {
    return $this->contactName;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactURL() {
    return $this->contactURL;
  }

  /**
   * {@inheritdoc}
   */
  public function getContactPhone() {
    return $this->contactPhone;
  }

  /**
   * {@inheritdoc}
   */
  public function getPartners() {
    return $this->partners;
  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedCode() {
    return $this->relatedCode;
  }

  /**
   * {@inheritdoc}
   */
  public function getReusedCode() {
    return $this->reusedCode;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataLastUpdated() {
    return $this->metadataLastUpdated;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastModified() {
    return $this->lastModified;
  }



  /**
   * {@inheritdoc}
   */
  public function setRepositoryURL($repositoryURL) {
    $this->repositoryURL = $repositoryURL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * {@inheritdoc}
   */
  public function setUsageType($usageType) {
    $this->usageType = $usageType;
  }

  /**
   * {@inheritdoc}
   */
  public function setExemptionText($exemptionText) {
    $this->exemptionText = $exemptionText;
  }

  /**
   * {@inheritdoc}
   */
  public function setLaborHours($laborHours) {
    $this->laborHours = $laborHours;
  }

  /**
   * {@inheritdoc}
   */
  public function setTags($tags) {
    $this->tags = $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactEmail($contactEmail) {
    $this->contactEmail = $contactEmail;
  }

  /**
   * {@inheritdoc}
   */
  public function setLicenses($licenses) {
    $this->licenses = $licenses;
  }

  /**
   * {@inheritdoc}
   */
  public function setVersion($version) {
    $this->version = $version;
  }

  /**
   * {@inheritdoc}
   */
  public function setOrganization($organization) {
    $this->organization = $organization;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($releaseStatus) {
    $this->releaseStatus = $releaseStatus;
  }

  /**
   * {@inheritdoc}
   */
  public function setVcs($vcs) {
    $this->vcs = $vcs;
  }

  /**
   * {@inheritdoc}
   */
  public function setHomepageURL($homepageURL) {
    $this->homepageURL = $homepageURL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDownloadURL($downloadURL) {
    $this->downloadURL = $downloadURL;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisclaimerText($disclaimerText) {
    $this->disclaimerText = $disclaimerText;
  }

  /**
   * {@inheritdoc}
   */
  public function setDisclaimerURL($disclaimerURL) {
    $this->disclaimerURL = $disclaimerURL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLanguages($languages) {
    $this->languages = $languages;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactName($contactName) {
    $this->contactName = $contactName;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactURL($contactURL) {
    $this->contactURL = $contactURL;
  }

  /**
   * {@inheritdoc}
   */
  public function setContactPhone($contactPhone) {
    $this->contactPhone = $contactPhone;
  }

  /**
   * {@inheritdoc}
   */
  public function setPartners($partners) {
    $this->partners = $partners;
  }

  /**
   * {@inheritdoc}
   */
  public function setRelatedCode($relatedCode) {
    $this->relatedCode = $relatedCode;
  }

  /**
   * {@inheritdoc}
   */
  public function setReusedCode($reusedCode) {
    $this->reusedCode = $reusedCode;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreated($created) {
    $this->created = $created;
  }

  /**
   * {@inheritdoc}
   */
  public function setmetadataLastUpdated($metadataLastUpdated) {
    $this->metadataLastUpdated = $metadataLastUpdated;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastModified($lastModified) {
    $this->lastModified = $lastModified;
  }

}
