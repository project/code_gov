<?php

/**
 * @file
 * Form handler for the code_gov_release add and edit forms.
 */

namespace Drupal\code_gov\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Render\Element;

/**
 * Form handler for the code_gov_release add and edit forms.
 */
class CodeGovReleaseForm extends EntityForm {

  /**
   * Constructs a CodeGovReleaseForm object.
   *
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The entity query.
   */
  public function __construct(QueryFactory $entity_query) {
    $this->entityQuery = $entity_query;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.query')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $release = $this->entity;

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#maxlength' => 255,
      '#default_value' => $release->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $release->id(),
      '#machine_name' => [
        'exists' => [$this, 'exist'],
      ],
      '#disabled' => !$release->isNew(),
    ];

    $details = array('@details' => 'https://code.gov/#/policy-guide/docs/compliance/inventory-code');

    $form['#tree'] = TRUE;
    $form_state->setCached(FALSE);

    $form['repositoryURL'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Repository URL'),
      '#description' => $this->t('The URL of the public release repository for open source repositories. This field is not required for repositories that are only available as government-wide reuse or are closed (pursuant to one of the exemptions).'),
      '#default_value' => $release->getRepositoryURL(),
    );

    $form['description'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('A one- or two-sentence description of the release.'),
      '#default_value' => $release->getDescription(),
      '#required' => TRUE,
    );

    $form['usageType'] = array(
      '#type' => 'select',
      '#title' => $this->t('Usage type'),
      '#description' => $this->t('Describes the usage permissions for the release. For details see <a target="_blank" href="@details">here</a>.', $details),
      '#default_value' => $release->getUsageType(),
      //'#required' => TRUE,
      '#options' => array(
        'openSource' => $this->t('Open source'),
        'governmentWideReuse' => $this->t('Government-wide reuse'),
        'exemptByLaw' => $this->t('Exempt by Law'),
        'exemptByNationalSecurity' => $this->t('Exempt by National Security'),
        'exemptByAgencySystem' => $this->t('Exempt by Agency System'),
        'exemptByAgencyMission' => $this->t('Exempt by Agency Mission'),
        'exemptByCIO' => $this->t('Exempt by CIO'),
      ),
    );

    $form['exemptionText'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Exemption text'),
      '#description' => $this->t('If an exemption is listed in the "usageType" field, this field should include a one- or two- sentence justification for the exemption used.'),
      '#default_value' => $release->getExemptionText(),
      '#states' => array(
        'visible' => array(
          array(':input[name="usageType"]' => array('value' => 'exemptByLaw')),
          array(':input[name="usageType"]' => array('value' => 'exemptByNationalSecurity')),
          array(':input[name="usageType"]' => array('value' => 'exemptByAgencySystem')),
          array(':input[name="usageType"]' => array('value' => 'exemptByAgencyMission')),
          array(':input[name="usageType"]' => array('value' => 'exemptByCIO')),
        ),
      ),
    );

    $form['laborHours'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Labor hours'),
      '#description' => $this->t('An estimate of total labor hours spent by your organization/component across all versions of this release.'),
      '#default_value' => $release->getLaborHours(),
      '#required' => TRUE,
    );

    $form['tags'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Tags'),
      '#description' => $this->t('Keywords that will be helpful in discovering and searching for the release. Enter one per line.'),
      '#default_value' => $release->getTags(),
      '#required' => TRUE,
    );

    $form['contactEmail'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Contact email'),
      '#description' => $this->t('An email address to contact the release.'),
      '#default_value' => $release->getContactEmail(),
      '#required' => TRUE,
    );

    $form['licenses'] = $this->multiWidget('licenses', $release->getLicenses(), array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#description' => t("An abbreviation for the name of the license. For example, 'CC0' or 'MIT.'"),
      ),
      'URL' => array(
        '#type' => 'textfield',
        '#title' => $this->t('URL'),
        '#description' => $this->t('The URL of the release license, if available.'),
      ),
      '#title' => $this->t('Licenses'),
      '#description' => $this->t('License details, if available.'),
    ), $form_state);

    $form['version'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Version'),
      '#description' => $this->t('The version for this release. For example, "1.0.0."'),
      '#default_value' => $release->getVersion(),
    );

    $form['organization'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Organization'),
      '#description' => $this->t('The organization or component within the agency that the releases listed belong to.'),
      '#default_value' => $release->getOrganization(),
    );

    $form['status'] = array(
      '#type' => 'select',
      '#title' => $this->t('Status'),
      '#description' => $this->t('The development status of the release. For details see <a target="_blank" href="@details">here</a>.', $details),
      '#default_value' => $release->getStatus(),
      '#options' => array(
        'Ideation' => 'Ideation',
        'Development' => 'Development',
        'Alpha' => 'Alpha',
        'Beta' => 'Beta',
        'Release Candidate' => 'Release Candidate',
        'Production' => 'Production',
        'Archival' => 'Archival',
      ),
    );

    $form['vcs'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Version control system'),
      '#description' => $this->t('A lowercase string with the name of the version control system that is being used for the release.'),
      '#default_value' => $release->getVcs(),
    );

    $form['homepageURL'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Homepage URL'),
      '#description' => $this->t('The URL of the public release homepage.'),
      '#default_value' => $release->getHomepageURL(),
    );

    $form['downloadURL'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Download URL'),
      '#description' => $this->t('The URL where a distribution of the release can be found.'),
      '#default_value' => $release->getDownloadURL(),
    );

    $form['disclaimerText'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Disclaimer text'),
      '#description' => $this->t('Short paragraph that includes disclaimer language to accompany the release.'),
      '#default_value' => $release->getDisclaimerText(),
    );

    $form['disclaimerURL'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Disclaimer URL'),
      '#description' => $this->t('The URL where disclaimer language regarding the release can be found.'),
      '#default_value' => $release->getDisclaimerURL(),
    );

    $form['languages'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Languages'),
      '#description' => $this->t('A list of strings with the names of the programming languages in use on the release. Enter one per line.'),
      '#default_value' => $release->getLanguages(),
    );

    $form['contactName'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Contact name'),
      '#description' => $this->t('The name of a contact or department for the release.'),
      '#default_value' => $release->getContactName(),
    );

    $form['contactURL'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Contact URL'),
      '#description' => $this->t('The URL to a website that can be used to reach the point of contact.'),
      '#default_value' => $release->getContactURL(),
    );

    $form['contactPhone'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Contact phone'),
      '#description' => $this->t('The phone number to contact a release.'),
      '#default_value' => $release->getContactPhone(),
    );

    $form['partners'] = $this->multiWidget('partners', $release->getPartners(), array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#description' => $this->t('The acronym describing the partner agency.'),
      ),
      'email' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Email'),
        '#description' => $this->t('The email address for the point of contact at the partner agency.'),
      ),
      '#title' => $this->t('Partners'),
      '#description' => $this->t('Each agency partnering on the release.'),
    ), $form_state);

    $form['relatedCode'] = $this->multiWidget('relatedCode', $release->getRelatedCode(), array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Code name'),
        '#description' => $this->t('The name of the code repository, project, library or release.'),
      ),
      'URL' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Code URL'),
        '#description' => $this->t('The URL where the code repository, project, library or release can be found.'),
      ),
      '#title' => $this->t('Related code'),
      '#description' => t("Affiliated government repositories that may be a part of the same project. For example, relatedCode for 'code-gov-web' would include 'code-gov-api' and 'code-gov-tools'"),
    ), $form_state);

    $form['reusedCode'] = $this->multiWidget('reusedCode', $release->getReusedCode(), array(
      'name' => array(
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#description' => $this->t('The name of the software used in this release.'),
      ),
      'URL' => array(
        '#type' => 'textfield',
        '#title' => $this->t('URL'),
        '#description' => $this->t('The URL where the software can be found.'),
      ),
      '#title' => $this->t('Reused code'),
      '#description' => $this->t('A list of government source code, libraries, frameworks, APIs, platforms or other software used in this release.'),
    ), $form_state);

    $date_generic = array(
      '#type' => 'textfield',
    );
    if (\Drupal::moduleHandler()->moduleExists('date')) {
      $date_generic = array(
        '#type' => 'date_select',
        '#date_type' => DATE_DATETIME,
        '#date_timezone' => date_default_timezone(),
        '#date_format' => 'Y-m-d',
        '#date_increment' => 1,
        '#date_year_range' => '-5:+5',
      );
    }
    if (\Drupal::moduleHandler()->moduleExists('date_popup')) {
      $date_generic['#type'] = 'date_popup';
    }

    $form['created'] = array(
      '#title' => $this->t('Created'),
      '#description' => $this->t('The date the release was created, in the format: YYYY-MM-DD'),
      '#default_value' => $release->getCreated(),
    ) + $date_generic;

    $form['lastModified'] = array(
      '#title' => $this->t('Last modified'),
      '#description' => $this->t('The date the release was modified, in the format: YYYY-MM-DD'),
      '#default_value' => $release->getLastModified(),
    ) + $date_generic;

    return $form;
  }

  /**
   * Helper function to generate multivalue widgets.
   */
  private function multiWidget($identifier, $raw_data, $config, &$form_state) {

    // Split out the config into the form API array for the fieldset ($fieldset)
    // and the element blueprints that will be used for each value ($elements).
    $fieldset = array();
    $elements = array();
    foreach (Element::children($config) as $element) {
      $elements[$element] = $config[$element];
      unset($config[$element]);
    }
    $fieldset = $config;
    $fieldset['#type'] = 'fieldset';
    $fieldset['#collapsible'] = TRUE;
    $fieldset['#collapsed'] = FALSE;

    // For AJAX purposes, put a wrapper around the fieldset.
    $fieldset_id = $identifier . '-fieldset-wrapper';
    $fieldset['#prefix'] = '<div id="' . $fieldset_id . '">';
    $fieldset['#suffix'] = '</div>';

    // Figure out how many values to print.
    $counter = 'num_' . $identifier;
    $num_items = $form_state->get($counter);
    if ($num_items === NULL) {
      $form_state->set($counter, 0);
      $num_items = 0;
    }

    // Do we have existing data?
    $data = array();
    // @TODO: Fix this section.
    if (!empty($raw_data) && !empty($raw_data[0])) {
      $data = unserialize($raw_data);
      if (count($data) > $form_state->get($counter)) {
        $num_items = count($data);
        $form_state->set($counter, $num_items);
      }
    }

    // Set up a blueprint for the elements of each item.
    $elements_blueprint = array(
      '#type' => 'container',
      '#attributes' => array('class' => array('code_gov-multivalue-item')),
      'elements' => $elements,
    );

    for ($i = 0; $i < $num_items; $i++) {
      $elements_copy = $elements_blueprint;
      if (!empty($data[$i])) {
        foreach ($data[$i] as $key => $value) {
          $elements_copy['elements'][$key]['#default_value'] = $value;
        }
      }
      $fieldset[] = $elements_copy;
    }

    // Add an "Add" button.
    $fieldset['actions'] = [
      '#type' => 'actions',
    ];
    $fieldset['actions']['add_' . $identifier] = array(
      '#type' => 'submit',
      '#value' => $this->t('Add'),
      '#name' => $identifier . '_add',
      '#submit' => ['::addOne_' . $identifier],
      '#limit_validation_errors' => [],
      '#ajax' => [
        'callback' => '::addmoreCallback_' . $identifier,
        'wrapper' => $fieldset_id,
        'method' => 'replace',
        'effect' => 'fade',
      ],
    );

    return $fieldset;
  }

  // Ajax callbacks for our multivalue widgets.
  public function addmoreCallback_licenses(array &$form, FormStateInterface $form_state) {
    return $form['licenses'];
  }
  public function addmoreCallback_partners(array &$form, FormStateInterface $form_state) {
    return $form['partners'];
  }
  public function addmoreCallback_relatedCode(array &$form, FormStateInterface $form_state) {
    return $form['relatedCode'];
  }
  public function addmoreCallback_reusedCode(array &$form, FormStateInterface $form_state) {
    return $form['reusedCode'];
  }

  // Ajax callbacks for our multivalue widget "add" buttons.
  public function addOne_licenses(array &$form, FormStateInterface $form_state) {
    $form_state->set('num_licenses', $form_state->get('num_licenses') + 1);
    $form_state->setRebuild();
  }
  public function addOne_partners(array &$form, FormStateInterface $form_state) {
    $form_state->set('num_partners', $form_state->get('num_partners') + 1);
    $form_state->setRebuild();
  }
  public function addOne_relatedCode(array &$form, FormStateInterface $form_state) {
    $form_state->set('num_relatedCode', $form_state->get('num_relatedCode') + 1);
    $form_state->setRebuild();
  }
  public function addOne_reusedCode(array &$form, FormStateInterface $form_state) {
    $form_state->set('num_reusedCode', $form_state->get('num_reusedCode') + 1);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $release = $this->entity;
    $status = $release->save();

    if ($status) {
      drupal_set_message($this->t('Saved the %label release.', [
        '%label' => $release->label(),
      ]));
    }
    else {
      drupal_set_message($this->t('The %label release was not saved.', [
        '%label' => $release->label(),
      ]));
    }

    $form_state->setRedirect('entity.code_gov_release.collection');
  }

  /**
   * Helper function to check whether a release config entity exists.
   */
  public function exist($id) {
    $entity = $this->entityQuery->get('code_gov_release')
      ->condition('id', $id)
      ->execute();
    return (bool) $entity;
  }
}
