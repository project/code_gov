<?php

/**
 * @file
 * Interface for CodeGovRelease classes.
 */

namespace Drupal\code_gov;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a code_gov_release entity.
 */
interface CodeGovReleaseInterface extends ConfigEntityInterface {

  /**
   * Gets the repositoryURL for this release.
   *
   * @return string
   *   The repositoryURL.
   */
  public function getRepositoryURL();

  /**
   * Gets the description for this release.
   *
   * @return string
   *   The description.
   */
  public function getDescription();

  /**
   * Gets the usageType for this release.
   *
   * @return string
   *   The usageType.
   */
  public function getUsageType();

  /**
   * Gets the exemptionText for this release.
   *
   * @return string
   *   The exemptionText.
   */
  public function getExemptionText();

  /**
   * Gets the laborHours for this release.
   *
   * @return string
   *   The laborHours.
   */
  public function getLaborHours();

  /**
   * Gets the tags for this release.
   *
   * @return string
   *   The tags.
   */
  public function getTags();

  /**
   * Gets the contactEmail for this release.
   *
   * @return string
   *   The contactEmail.
   */
  public function getContactEmail();

  /**
   * Gets the licenses for this release.
   *
   * @return string
   *   The licenses.
   */
  public function getLicenses();

  /**
   * Gets the version for this release.
   *
   * @return string
   *   The version.
   */
  public function getVersion();

  /**
   * Gets the organization for this release.
   *
   * @return string
   *   The organization.
   */
  public function getOrganization();

  /**
   * Gets the status for this release.
   *
   * @return string
   *   The status.
   */
  public function getStatus();

  /**
   * Gets the vcs for this release.
   *
   * @return string
   *   The vcs.
   */
  public function getVcs();

  /**
   * Gets the homepageURL for this release.
   *
   * @return string
   *   The homepageURL.
   */
  public function getHomepageURL();

  /**
   * Gets the downloadURL for this release.
   *
   * @return string
   *   The downloadURL.
   */
  public function getDownloadURL();

  /**
   * Gets the disclaimerText for this release.
   *
   * @return string
   *   The disclaimerText.
   */
  public function getDisclaimerText();

  /**
   * Gets the disclaimerURL for this release.
   *
   * @return string
   *   The disclaimerURL.
   */
  public function getDisclaimerURL();

  /**
   * Gets the languages for this release.
   *
   * @return string
   *   The languages.
   */
  public function getLanguages();

  /**
   * Gets the contactName for this release.
   *
   * @return string
   *   The contactName.
   */
  public function getContactName();

  /**
   * Gets the contactURL for this release.
   *
   * @return string
   *   The contactURL.
   */
  public function getContactURL();

  /**
   * Gets the contactPhone for this release.
   *
   * @return string
   *   The contactPhone.
   */
  public function getContactPhone();

  /**
   * Gets the partners for this release.
   *
   * @return string
   *   The partners.
   */
  public function getPartners();

  /**
   * Gets the relatedCode for this release.
   *
   * @return string
   *   The relatedCode.
   */
  public function getRelatedCode();

  /**
   * Gets the reusedCode for this release.
   *
   * @return string
   *   The reusedCode.
   */
  public function getReusedCode();

  /**
   * Gets the created for this release.
   *
   * @return string
   *   The created.
   */
  public function getCreated();

  /**
   * Gets the metadataLastUpdated for this release.
   *
   * @return string
   *   The metadataLastUpdated.
   */
  public function getMetadataLastUpdated();

  /**
   * Gets the lastModified for this release.
   *
   * @return string
   *   The lastModified.
   */
  public function getLastModified();



  /**
   * Sets the repositoryURL for this release.
   *
   * @param string $repositoryURL
   *   The repositoryURL.
   */
  public function setRepositoryURL($repositoryURL);

  /**
   * Sets the description for this release.
   *
   * @param string $description
   *   The description.
   */
  public function setDescription($description);

  /**
   * Sets the usageType for this release.
   *
   * @param string $usageType
   *   The usageType.
   */
  public function setUsageType($usageType);

  /**
   * Sets the exemptionText for this release.
   *
   * @param string $exemptionText
   *   The exemptionText.
   */
  public function setExemptionText($exemptionText);

  /**
   * Sets the laborHours for this release.
   *
   * @param string $laborHours
   *   The laborHours.
   */
  public function setLaborHours($laborHours);

  /**
   * Sets the tags for this release.
   *
   * @param string $tags
   *   The tags.
   */
  public function setTags($tags);

  /**
   * Sets the contactEmail for this release.
   *
   * @param string $contactEmail
   *   The contactEmail.
   */
  public function setContactEmail($contactEmail);

  /**
   * Sets the licenses for this release.
   *
   * @param string $licenses
   *   The licenses.
   */
  public function setLicenses($licenses);

  /**
   * Sets the version for this release.
   *
   * @param string $version
   *   The version.
   */
  public function setVersion($version);

  /**
   * Sets the organization for this release.
   *
   * @param string $organization
   *   The organization.
   */
  public function setOrganization($organization);

  /**
   * Sets the status for this release.
   *
   * @param string $releaseStatus
   *   The status.
   */
  public function setStatus($releaseStatus);

  /**
   * Sets the vcs for this release.
   *
   * @param string $vcs
   *   The vcs.
   */
  public function setVcs($vcs);

  /**
   * Sets the homepageURL for this release.
   *
   * @param string $homepageURL
   *   The homepageURL.
   */
  public function setHomepageURL($homepageURL);

  /**
   * Sets the downloadURL for this release.
   *
   * @param string $downloadURL
   *   The downloadURL.
   */
  public function setDownloadURL($downloadURL);

  /**
   * Sets the disclaimerText for this release.
   *
   * @param string $disclaimerText
   *   The disclaimerText.
   */
  public function setDisclaimerText($disclaimerText);

  /**
   * Sets the disclaimerURL for this release.
   *
   * @param string $disclaimerURL
   *   The disclaimerURL.
   */
  public function setDisclaimerURL($disclaimerURL);

  /**
   * Sets the languages for this release.
   *
   * @param string $languages
   *   The languages.
   */
  public function setLanguages($languages);

  /**
   * Sets the contactName for this release.
   *
   * @param string $contactName
   *   The contactName.
   */
  public function setContactName($contactName);

  /**
   * Sets the contactURL for this release.
   *
   * @param string $contactURL
   *   The contactURL.
   */
  public function setContactURL($contactURL);

  /**
   * Sets the contactPhone for this release.
   *
   * @param string $contactPhone
   *   The contactPhone.
   */
  public function setContactPhone($contactPhone);

  /**
   * Sets the partners for this release.
   *
   * @param string $partners
   *   The partners.
   */
  public function setPartners($partners);

  /**
   * Sets the relatedCode for this release.
   *
   * @param string $relatedCode
   *   The relatedCode.
   */
  public function setRelatedCode($relatedCode);

  /**
   * Sets the reusedCode for this release.
   *
   * @param string $reusedCode
   *   The reusedCode.
   */
  public function setReusedCode($reusedCode);

  /**
   * Sets the created for this release.
   *
   * @param string $created
   *   The created.
   */
  public function setCreated($created);

  /**
   * Sets the metadataLastUpdated for this release.
   *
   * @param string $metadataLastUpdated
   *   The metadataLastUpdated.
   */
  public function setMetadataLastUpdated($metadataLastUpdated);

  /**
   * Sets the lastModified for this release.
   *
   * @param string $lastModified
   *   The lastModified.
   */
  public function setLastModified($lastModified);
}
