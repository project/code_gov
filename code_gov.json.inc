<?php

/**
 * @file
 * JSON page callbacks for code_gov.
 */

/**
 * Page callback for the code inventory in json form.
 */
function _code_gov_json() {
  if ($cache = cache_get('code_gov_json')) {
    // Try to use cached data first.
    return $cache->data;
  }

  // Start with the few general settings.
  $settings = variable_get('code_gov_settings', array());
  if (!empty($settings['measurementType'])) {
    if ('other' != $settings['measurementType']['method']) {
      unset($settings['measurementType']['ifOther']);
    }
  }

  // A few lists of attributes to help clean up the json before outputting it.
  $serialized_attributes = array(
    'licenses',
    'partners',
    'relatedCode',
    'reusedCode',
  );
  $textarea_array_attributes = array(
    'tags',
    'languages',
  );
  $date_attributes = array(
    'metadataLastUpdated',
    'lastModified',
    'created',
  );
  $drupal_attributes = array(
    'rid',
    'type',
    'table',
    'export_type',
  );
  $contact_attributes = array(
    'contactEmail',
    'contactName',
    'contactURL',
    'contactPhone',
  );
  $contact_attribute_fixes = array(
    'contactEmail' => 'email',
    'contactURL' => 'URL',
    'contactPhone' => 'phone',
    'contactName' => 'name',
  );
  $permissions_attributes = array(
    'licenses',
    'usageType',
    'exemptionText',
  );

  $releases_array = array();

  ctools_include('export');
  $releases = ctools_export_load_object('code_gov_release');

  foreach ($releases as $release) {
    // Unserialize serialized data.
    foreach ($serialized_attributes as $attribute) {
      $release->{$attribute} = unserialize($release->{$attribute});
    }
    // Clean up Drupal-specific stuff.
    foreach ($drupal_attributes as $attribute) {
      unset($release->{$attribute});
    }
    // Convert new-lines to arrays.
    foreach ($textarea_array_attributes as $attribute) {
      $release->{$attribute} = preg_split('/\r\n|[\r\n]/', $release->{$attribute});
    }
    // Fix the dates.
    $date = array();
    foreach ($date_attributes as $attribute) {
      if (!empty($release->{$attribute})) {
        $date[$attribute] = substr($release->{$attribute}, 0, 10);
        unset($release->{$attribute});
      }
    }
    $release->date = $date;
    // Fix the contact info.
    $contact = array();

    foreach ($contact_attributes as $attribute) {
      $fixed_attribute = $contact_attribute_fixes[$attribute];
      $contact[$fixed_attribute] = $release->{$attribute};
      unset($release->{$attribute});
    }
    $release->contact = $contact;
    // Fix the permissions object.
    $permissions = array();
    foreach ($permissions_attributes as $attribute) {
      $permissions[$attribute] = $release->{$attribute};
      unset($release->{$attribute});
    }
    // A quirky requirement of NULL if there are no licenses.
    if (empty($permissions['licenses'])) {
      $permissions['licenses'] = NULL;
    }
    $release->permissions = $permissions;

    // Make some variable type adjustments.
    $release->laborHours = intval($release->laborHours);

    // Finally add to the array of releases.
    $releases_array[] = $release;
  }

  $settings['releases'] = $releases_array;

  cache_set('code_gov_json', $settings, 'cache');

  return $settings;
}
