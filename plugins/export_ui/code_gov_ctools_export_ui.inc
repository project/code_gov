<?php

/**
 * @file
 * CTools forms for the Code.gov module.
 */

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'code_gov_release',
  'access' => 'administer code_gov',

  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/search',
    'menu item' => 'code-gov',
    'menu title' => 'Code.gov settings',
    'menu description' => 'Administer Code.gov releases.',
  ),

  // Define user interface texts.
  'title singular' => t('release'),
  'title plural' => t('releases'),
  'title singular proper' => t('Code.gov release'),
  'title plural proper' => t('Code.gov releases'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => '_code_gov_ctools_export_ui_form',
    'submit' => '_code_gov_ctools_export_ui_form_submit',
    'validate' => '_code_gov_ctools_export_ui_form_validate',
  ),
);

/**
 * Define the release add/edit form.
 */
function _code_gov_ctools_export_ui_form(&$form, &$form_state) {
  $release = $form_state['item'];
  $details = array('@details' => 'https://code.gov/#/policy-guide/docs/compliance/inventory-code');
  $form['#tree'] = TRUE;

  $form['repositoryURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Repository URL'),
    '#description' => t('The URL of the public release repository for open source repositories. This field is not required for repositories that are only available as government-wide reuse or are closed (pursuant to one of the exemptions).'),
    '#default_value' => $release->repositoryURL,
  );

  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#description' => t('A one- or two-sentence description of the release.'),
    '#default_value' => $release->description,
    '#required' => TRUE,
  );

  $form['usageType'] = array(
    '#type' => 'select',
    '#title' => t('Usage type'),
    '#description' => t('Describes the usage permissions for the release. For details see <a target="_blank" href="@details">here</a>.', $details),
    '#default_value' => $release->usageType,
    '#required' => TRUE,
    '#options' => array(
      'openSource' => t('Open source'),
      'governmentWideReuse' => t('Government-wide reuse'),
      'exemptByLaw' => t('Exempt by Law'),
      'exemptByNationalSecurity' => t('Exempt by National Security'),
      'exemptByAgencySystem' => t('Exempt by Agency System'),
      'exemptByAgencyMission' => t('Exempt by Agency Mission'),
      'exemptByCIO' => t('Exempt by CIO'),
    ),
  );

  $form['exemptionText'] = array(
    '#type' => 'textarea',
    '#title' => t('Exemption text'),
    '#description' => t('If an exemption is listed in the "usageType" field, this field should include a one- or two- sentence justification for the exemption used.'),
    '#default_value' => $release->exemptionText,
    '#states' => array(
      'visible' => array(
        array(':input[name="usageType"]' => array('value' => 'exemptByLaw')),
        array(':input[name="usageType"]' => array('value' => 'exemptByNationalSecurity')),
        array(':input[name="usageType"]' => array('value' => 'exemptByAgencySystem')),
        array(':input[name="usageType"]' => array('value' => 'exemptByAgencyMission')),
        array(':input[name="usageType"]' => array('value' => 'exemptByCIO')),
      ),
    ),
  );

  $form['laborHours'] = array(
    '#type' => 'textfield',
    '#title' => t('Labor hours'),
    '#description' => t('An estimate of total labor hours spent by your organization/component across all versions of this release.'),
    '#default_value' => $release->laborHours,
    '#required' => TRUE,
    '#element_validate' => array('element_validate_integer_positive'),
  );

  $form['tags'] = array(
    '#type' => 'textarea',
    '#title' => t('Tags'),
    '#description' => t('Keywords that will be helpful in discovering and searching for the release. Enter one per line.'),
    '#default_value' => $release->tags,
    '#required' => TRUE,
  );

  $form['contactEmail'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact email'),
    '#description' => t('An email address to contact the release.'),
    '#default_value' => $release->contactEmail,
    '#required' => TRUE,
  );

  $form['licenses'] = _code_gov_multi_widget('licenses', $release->licenses, array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t("An abbreviation for the name of the license. For example, 'CC0' or 'MIT.'"),
    ),
    'URL' => array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('The URL of the release license, if available.'),
    ),
    '#title' => t('Licenses'),
    '#description' => t('License details, if available.'),
  ), $form_state);

  $form['version'] = array(
    '#type' => 'textfield',
    '#title' => t('Version'),
    '#description' => t('The version for this release. For example, "1.0.0."'),
    '#default_value' => $release->version,
  );

  $form['organization'] = array(
    '#type' => 'textfield',
    '#title' => t('Organization'),
    '#description' => t('The organization or component within the agency that the releases listed belong to.'),
    '#default_value' => $release->organization,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('The development status of the release. For details see <a target="_blank" href="@details">here</a>.', $details),
    '#default_value' => $release->status,
    '#options' => array(
      'Ideation' => 'Ideation',
      'Development' => 'Development',
      'Alpha' => 'Alpha',
      'Beta' => 'Beta',
      'Release Candidate' => 'Release Candidate',
      'Production' => 'Production',
      'Archival' => 'Archival',
    ),
  );

  $form['vcs'] = array(
    '#type' => 'textfield',
    '#title' => t('Version control system'),
    '#description' => t('A lowercase string with the name of the version control system that is being used for the release.'),
    '#default_value' => $release->vcs,
  );

  $form['homepageURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Homepage URL'),
    '#description' => t('The URL of the public release homepage.'),
    '#default_value' => $release->homepageURL,
  );

  $form['downloadURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Download URL'),
    '#description' => t('The URL where a distribution of the release can be found.'),
    '#default_value' => $release->downloadURL,
  );

  $form['disclaimerText'] = array(
    '#type' => 'textarea',
    '#title' => t('Disclaimer text'),
    '#description' => t('Short paragraph that includes disclaimer language to accompany the release.'),
    '#default_value' => $release->disclaimerText,
  );

  $form['disclaimerURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Disclaimer URL'),
    '#description' => t('The URL where disclaimer language regarding the release can be found.'),
    '#default_value' => $release->disclaimerURL,
  );

  $form['languages'] = array(
    '#type' => 'textarea',
    '#title' => t('Languages'),
    '#description' => t('A list of strings with the names of the programming languages in use on the release. Enter one per line.'),
    '#default_value' => $release->languages,
  );

  $form['contactName'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact name'),
    '#description' => t('The name of a contact or department for the release.'),
    '#default_value' => $release->contactName,
  );

  $form['contactURL'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact URL'),
    '#description' => t('The URL to a website that can be used to reach the point of contact.'),
    '#default_value' => $release->contactURL,
  );

  $form['contactPhone'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact phone'),
    '#description' => t('The phone number to contact a release.'),
    '#default_value' => $release->contactPhone,
  );

  $form['partners'] = _code_gov_multi_widget('partners', $release->partners, array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The acronym describing the partner agency.'),
    ),
    'email' => array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#description' => t('The email address for the point of contact at the partner agency.'),
    ),
    '#title' => t('Partners'),
    '#description' => t('Each agency partnering on the release.'),
  ), $form_state);

  $form['relatedCode'] = _code_gov_multi_widget('relatedCode', $release->relatedCode, array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Code name'),
      '#description' => t('The name of the code repository, project, library or release.'),
    ),
    'URL' => array(
      '#type' => 'textfield',
      '#title' => t('Code URL'),
      '#description' => t('The URL where the code repository, project, library or release can be found.'),
    ),
    '#title' => t('Related code'),
    '#description' => t("Affiliated government repositories that may be a part of the same project. For example, relatedCode for 'code-gov-web' would include 'code-gov-api' and 'code-gov-tools'"),
  ), $form_state);

  $form['reusedCode'] = _code_gov_multi_widget('reusedCode', $release->reusedCode, array(
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#description' => t('The name of the software used in this release.'),
    ),
    'URL' => array(
      '#type' => 'textfield',
      '#title' => t('URL'),
      '#description' => t('The URL where the software can be found.'),
    ),
    '#title' => t('Reused code'),
    '#description' => t('A list of government source code, libraries, frameworks, APIs, platforms or other software used in this release.'),
  ), $form_state);

  $date_generic = array(
    '#type' => 'textfield',
  );
  if (module_exists('date')) {
    $date_generic = array(
      '#type' => 'date_select',
      '#date_type' => DATE_DATETIME,
      '#date_timezone' => date_default_timezone(),
      '#date_format' => 'Y-m-d',
      '#date_increment' => 1,
      '#date_year_range' => '-5:+5',
    );
  }
  if (module_exists('date_popup')) {
    $date_generic['#type'] = 'date_popup';
  }

  $form['created'] = array(
    '#title' => t('Created'),
    '#description' => t('The date the release was created, in the format: YYYY-MM-DD'),
    '#default_value' => $release->created,
  ) + $date_generic;

  $form['lastModified'] = array(
    '#title' => t('Last modified'),
    '#description' => t('The date the release was modified, in the format: YYYY-MM-DD'),
    '#default_value' => $release->lastModified,
  ) + $date_generic;

  $path = drupal_get_path('module', 'code_gov');
  $form['#attached']['css'][] = $path . '/plugins/export_ui/code_gov.admin.css';
}

function _code_gov_multi_widget($identifier, $raw_data, $config, &$form_state) {

  // Split out the config into the form API array for the fieldset ($fieldset)
  // and the element blueprints that will be used for each value ($elements).
  $fieldset = array();
  $elements = array();
  foreach (element_children($config) as $element) {
    $elements[$element] = $config[$element];
    unset($config[$element]);
  }
  $fieldset = $config;
  $fieldset['#type'] = 'fieldset';
  $fieldset['#collapsible'] = TRUE;
  $fieldset['#collapsed'] = FALSE;

  // For AJAX purposes, put a wrapper around the fieldset.
  $fieldset_id = $identifier . '-fieldset-wrapper';
  $fieldset['#prefix'] = '<div id="' . $fieldset_id . '">';
  $fieldset['#suffix'] = '</div>';

  // Figure out how many values to print.
  $counter = 'num_' . $identifier;
  if (empty($form_state[$counter])) {
    $form_state[$counter] = 0;
  }

  // Do we have existing data?
  $data = array();
  if (!empty($raw_data)) {
    $data = unserialize($raw_data);
    if (count($data) > $form_state[$counter]) {
      $form_state[$counter] = count($data);
    }
  }

  // Set up a blueprint for the elements of each item.
  $elements_blueprint = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('code_gov-multivalue-item')),
    'elements' => $elements,
  );

  for ($i = 0; $i < $form_state[$counter]; $i++) {
    $elements_copy = $elements_blueprint;
    if (!empty($data[$i])) {
      foreach ($data[$i] as $key => $value) {
        $elements_copy['elements'][$key]['#default_value'] = $value;
      }
    }
    $fieldset[] = $elements_copy;
  }

  // Add an "Add" button.
  $fieldset['add_' . $identifier] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#name' => $identifier . '_add',
    '#submit' => array('_code_gov_' . $identifier . '_add'),
    '#ajax' => array(
      'callback' => '_code_gov_' . $identifier . '_callback',
      'wrapper' => $fieldset_id,
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return $fieldset;
}

// Ajax callbacks for our multivalue widgets.
function _code_gov_licenses_callback($form, $form_state) {
  return $form['licenses'];
}
function _code_gov_partners_callback($form, $form_state) {
  return $form['partners'];
}
function _code_gov_relatedCode_callback($form, $form_state) {
  return $form['relatedCode'];
}
function _code_gov_reusedCode_callback($form, $form_state) {
  return $form['reusedCode'];
}

// Ajax callbacks for our multivalue widget "add" buttons.
function _code_gov_licenses_add($form, &$form_state) {
  $form_state['num_licenses'] += 1;
  $form_state['rebuild'] = TRUE;
}
function _code_gov_partners_add($form, &$form_state) {
  $form_state['num_partners'] += 1;
  $form_state['rebuild'] = TRUE;
}
function _code_gov_relatedCode_add($form, &$form_state) {
  $form_state['num_relatedCode'] += 1;
  $form_state['rebuild'] = TRUE;
}
function _code_gov_reusedCode_add($form, &$form_state) {
  $form_state['num_reusedCode'] += 1;
  $form_state['rebuild'] = TRUE;
}

function _code_gov_ctools_export_ui_form_validate($form, $form_state) {
  // @TODO: Add some validation.
}

function _code_gov_ctools_export_ui_form_submit($form, &$form_state) {
  // Deal with our multivalue widgets.
  $multivalue_widgets = array(
    'licenses',
    'partners',
    'relatedCode',
    'reusedCode',
  );
  foreach ($multivalue_widgets as $multivalue_widget) {
    $parsed = array();
    if (!empty($form_state['input'][$multivalue_widget])) {
      $parsed = _code_gov_parse_multivalue_input($form_state['input'][$multivalue_widget]);
    }
    $form_state['values'][$multivalue_widget] = serialize($parsed);
  }

  $release = $form_state['item'];

  // Automatically update the metadataLastUpdated setting.
  $release->metadataLastUpdated = date('Y-m-d');

  // I don't understand why this is necessary, but this avoids the
  // Ctools exportable being saved without a "name".
  if (empty($release->name)) {
    $release->name = $form_state['values']['info']['name'];
  }

  // Make sure the date columns are not strings.
  $date_columns = array('created', 'lastModified');
  foreach ($date_columns as $date_column) {
    if (empty($form_state['values'][$date_column])) {
      unset($form_state['values'][$date_column]);
      unset($release->{$date_column});
    }
  }

  // Make sure to clear the cached version.
  cache_clear_all('code_gov_json', 'cache', TRUE);
}

function _code_gov_parse_multivalue_input($input) {
  $parsed = array();
  foreach ($input as $item) {
    $item_empty = TRUE;
    foreach ($item['elements'] as $key => $value) {
      if (!empty($value)) {
        $item_empty = FALSE;
      }
    }
    if (!$item_empty) {
      $parsed[] = $item['elements'];
    }
  }
  return $parsed;
}
